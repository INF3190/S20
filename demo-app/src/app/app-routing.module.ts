import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdulteComponent } from './adulte/adulte.component';
import { AineComponent } from './aine/aine.component';

const routes: Routes = [
  { path: 'adultes', component: AdulteComponent },
  { path: 'aines', component: AineComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
