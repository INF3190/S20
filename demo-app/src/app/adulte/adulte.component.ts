import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { DonneesService,NoteInfo } from '../donnees.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Observable, throwError } from 'rxjs';

@Component({
  selector: 'app-adulte',
  templateUrl: './adulte.component.html',
  styleUrls: ['./adulte.component.css']
})
export class AdulteComponent implements OnInit, AfterViewInit {
  columnsToDisplay = ['prenom', 'nom', 'age']; // colonnes affichees
  columnsToDisplay2 = ['age','prenom'];//colonnes affichees
  dataSource = new MatTableDataSource<PersonInfo>(this.donneesService.jeunes());
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource2 = new MatTableDataSource<PersonInfo>(this.donneesService.personnes);

  personnes: any[] = this.donneesService.personnes;
  notes:any = [];
  constructor(private donneesService: DonneesService) { }

  ngOnInit(): void {

  }

  getJeunes():any[]{
    return this.donneesService.jeunes();

  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  nbmajeurs(): number{
    let nb = 0;

   nb = this.donneesService.nbaines();
    return nb;
   }

  resetNotes(){
    this.notes = [];
  }
  getNotes() {
    this.donneesService.getNotes().subscribe((data: NoteInfo[]) => {this.notes=data });

  }
}
//format info des personnes
export interface PersonInfo{
  nom: string;
  prenom: string;
  age: Number;
}
