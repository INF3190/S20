import { Component } from '@angular/core';
import { DonneesService } from './donnees.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'service Angular et appel http --S20';
  constructor(private donneesService: DonneesService){}

  getListe():any[]{
     return this.donneesService.personnes;
  }
}
