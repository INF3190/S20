import { Component, OnInit } from '@angular/core';
import { DonneesService } from '../donnees.service';
@Component({
  selector: 'app-aine',
  templateUrl: './aine.component.html',
  styleUrls: ['./aine.component.css']
})
export class AineComponent implements OnInit {
  nb = this.donneesService.nbaines();
  constructor(private donneesService: DonneesService) { }

  ngOnInit(): void {
  }

  getpersinfo() {
    return this.donneesService.personnes;
  }

}
