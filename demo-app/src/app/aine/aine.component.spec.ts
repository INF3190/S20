import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AineComponent } from './aine.component';

describe('AineComponent', () => {
  let component: AineComponent;
  let fixture: ComponentFixture<AineComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
