import { Injectable } from '@angular/core';
import * as liste from '../assets/data/liste.json';
//ajout http
import { HttpClient, HttpHeaders, HttpClientModule, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class DonneesService {

  personnes:any = (liste as any).default;// default est une clé qui s'ajoute par defaut pour un tableau sans clé

  constructor(private http : HttpClient) { }

  jeunes():any[]{

    let returnval:any[]=[];
    let i=0;
    for (let pers of this.personnes){
      if((pers.age>=18) && (pers.age<70)){
       // pers.ecole='UQAM';// exemple ajout un champ a lsuppelemntaire
      returnval[i]=pers;
      i = i +1;
      }
    }
    return (returnval);
  }

  nbaines(): number{
    let nb = 0;
    for (let pers of this.personnes) {
      if (pers.age >= 70) {
        nb++;
      }
    }
    return nb;
  }

  getNotes():Observable<NoteInfo[]> {

    let url: string = "http://localhost:3000/getjson?f=notes.json";
    return this.http.get<NoteInfo[]>(url, { "responseType": "json" });

  }
}

export interface NoteInfo{
  codepermanent: string;
  note: string;
}
